# Hello World CBun

Hello World CBun represents the simplest CBunX application. It uses Kotlin and Jetpack Compose
to show customizable Hello World text. Above that it can also serve as an inspiration for the 
default configuration of Android project with CBunX support. 

This project is the end result of a step-by-step tutorial, so if you prefer to do it step by step,
please refer to the [First CBun App](https://kassowrobots.gitlab.io/cbunx-api-doc/first_cbun_app/first_cbun_app.html) document.

![Hello World CBun](printscreen.png)

The Hello World CBun is written in Kotlin and based on the [CBunX Framework](https://kassowrobots.gitlab.io/cbunx-api-doc/).

## Frontend Build Environment

The Hello World CBun comes with the preconfigured frontend build environment which is based on the Android Studio. With this you can open and debug the CBun frontend on Windows, MacOS or Linux without the need for complicated build environment configuration. 

### Prerequisities

To get started, follow these steps:

1. Install the [Android Studio](https://developer.android.com/studio).

### Frontend Emulator Setup

By using the emulator (virtual device) you can test your UI directly on your development machine within the Android Studio. You need to configure the virtual device just once for all of your CBun frontend projects. To setup the emulator, follow these steps.

1. Open the CBun project folder in Android Studio. 

2. Open **Tools** -> **Device Manager** to configure an emulated virtual device for the standalone application test. Click **Create device** to add new virtual device.

3. Click **New Hardware Profile** to create new emulator profile, that will match the UI behaviour of the real robot in terms of the resolution and the density of pixels.

4. Enter the device name (for example CBun App Emulator) and configure the emulator resolution (932 x 987 px) and screen size (6.2”) to fit the CBun App container in the Teach Pendant host app. The click the **Finish** button.

5. Once the new hardware profile is added, select it and click the **Next** button.

6. Select the **Q** system image, since Android 10 ensures compatibility with the highest amount of our robots. Then click the **Next** button.

7. Check the device configuration, enter the AVD Name and click the **Finish** button.

8. Finally click the run app button (or Ctrl-R shortcut) to launch your application in the emulator.

## CBun Assembly

The resulting cbun is included in the root project folder. For building the cbun yourself, change to the root project folder and follow these steps:

```
mkdir assemble
cd assemble
mkdir CBunHelloWorld
cp ../bundle.xml ./CBunHelloWorld
cp ../app/build/outputs/apk/debug/app-debug.apk ./CBunHelloWorld/cbunhelloworld.apk
tar -cvzf ../cbunhelloworld.cbun -C ./CBunHelloWorld .
cd ..
rm -rf assemble
```